import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


ax = None


def init_plot():
    global ax
    plt.rc('text', usetex=True)
    plt.style.use("seaborn-whitegrid")
    fig = plt.figure()

    fig, ax = plt.subplots()
    ax.grid("on")

    ax.set_xlim(-5, 5)
    ax.set_ylim(-0.1, 1.1)
    ax.set_aspect(10 / 1.2)


def sigmoid_plot():
    init_plot()
    labels = []
    titles = []

    x = np.linspace(-10, 10, 1000)
    y = sigmoid(1 * x)
    color = "#009688CF"
    plt.plot(x, y, c=color)
    labels.append(Line2D([0], [0], color=color, lw=1.5))
    titles.append(r'$\sigma (x)$')

    ax.legend(labels, titles, loc="upper left")
    plt.savefig("./images/sigmoid.svg")


if __name__ == "__main__":
    sigmoid_plot()
