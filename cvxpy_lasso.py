import os
import cvxpy as cp
import numpy as np
import torch
import matplotlib.pyplot as plt
import time
import datetime
import data_vpr
from pathlib import Path
from extractor import Container
import shell_eval


def generate_data(m=100, n=20, sigma=5):
    "Generates data matrix X and observations Y."
    np.random.seed(1)
    beta_star = np.random.randn(n)
    # Generate an ill-conditioned data matrix
    X = np.random.randn(m, n)
    # Corrupt the observations with additive Gaussian noise
    Y = X.dot(beta_star) + np.random.normal(0, sigma, size=m)
    return X, Y


def loss_fn(X, Y, beta):
    return cp.pnorm(X @ beta - Y, p=2) ** 2


def regularizer(beta):
    # ridge
    #return cp.pnorm(beta, p=2) ** 2

    # lasso
    return cp.norm1(beta)


def objective_fn(X, Y, beta, lambd):
    return loss_fn(X, Y, beta) + lambd * regularizer(beta)


def mse(X, Y, beta):
    return 1.0 / X.shape[0] * loss_fn(X, Y, beta).value


def cvxpy_example():
    m = 50
    n = 20
    sigma = 5

    X, Y = generate_data(m, n, sigma)
    X_train = X[:25, :]
    Y_train = Y[:25]
    X_test = X[25:, :]
    Y_test = Y[25:]

    beta = cp.Variable(n)
    lambd = cp.Parameter(nonneg=True)

    problem = cp.Problem(cp.Minimize(objective_fn(X_train, Y_train, beta, lambd)))

    lambd_values = np.logspace(-2, 3, 50)
    train_errors = []
    test_errors = []
    beta_values = []
    for v in lambd_values:
        t1 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        lambd.value = v
        problem.solve()
        train_errors.append(mse(X_train, Y_train, beta))
        test_errors.append(mse(X_test, Y_test, beta))
        beta_values.append(beta.value)
        t2 = time.time()
        print("{}-{}".format(t1, t2))
    print(beta_values.__len__())
    b = beta_values[0]
    #b_array = np.empty()
    print(type(b))
    b_array = np.empty((lambd_values.shape[0], n))
    print(b_array.shape)

    for betaa in beta_values:
        print(betaa)
        print(betaa.shape)


    def plot_train_test_errors(train_errors, test_errors, lambd_values):
        plt.plot(lambd_values, train_errors, label="Train error")
        plt.plot(lambd_values, test_errors, label="Test error")
        plt.xscale("log")
        plt.legend(loc="upper left")
        plt.xlabel(r"$\lambda$", fontsize=16)
        plt.title("Mean Squared Error (MSE)")
        plt.show()

    plot_train_test_errors(train_errors, test_errors, lambd_values)


    def plot_regularization_path(lambd_values, beta_values):

        plt.rc('text', usetex=True)
        plt.style.use("seaborn-whitegrid")
        fig = plt.figure()

        fig, ax = plt.subplots()
        ax.grid("on")

        #ax.set_xlim(-5, 5)
        #ax.set_ylim(-0.1, 1.1)
        #ax.set_aspect(10 / 1.2)
        #ax.set_aspect(1000 / 900)

        num_coeffs = len(beta_values[0])
        for i in range(num_coeffs):
            plt.plot(lambd_values, [wi[i] for wi in beta_values])
        plt.xlabel(r"$\lambda$", fontsize=16)
        plt.ylabel(r"$\theta_i$", fontsize=16)
        plt.xscale("log")
        #plt.savefig("lasso.svg")
        plt.show()
    plot_regularization_path(lambd_values, beta_values)


def lasso_vpr(root="./weights/vpr_lasso_3/", to_save=True, lambd_values=None):
    Path(root).mkdir(parents=True, exist_ok=True)
    X, y = data_vpr.lasso_regression_data(dataset_name="bonn")
    beta = cp.Variable(X.shape[1])

    lambd = cp.Parameter(nonneg=True)
    problem = cp.Problem(cp.Minimize(objective_fn(X, y, beta, lambd)))
    if lambd_values is None:
        lambd_values = np.logspace(-2, 2, 40)

    for k, lambd_value in enumerate(lambd_values):
        lambd.value = lambd_value
        problem.solve()
        if to_save:
            np.save(root + "{}.npy".format(k), beta.value)


def lasso_test():
    root = "./weights/vpr_lasso/"
    img_root = "./imgs/vpr_lasso/"
    os.system("mkdir -p {}".format(img_root))

    #Q, D, gt = data_vpr.vpr_data("freiburg")
    Q, D, gt = data_vpr.vpr_data("bonn")

    Q.div_(torch.norm(Q, 2, 1, True))
    D.div_(torch.norm(D, 2, 1, True))

    plt.imsave(img_root + "40.png", (D @ Q.transpose(0, 1)).numpy())

    exit()
    lambd_values = np.logspace(-2, 2, 40)
    for k in range(0, 40):
        beta = torch.from_numpy(np.load(root + "{}.npy".format(k)))

        Q_fs = torch.mul(Q, beta.repeat((Q.shape[0], 1)))
        D_fs = torch.mul(D, beta.repeat((D.shape[0], 1)))

        Q_fs.div_(torch.norm(Q_fs, 2, 1, True))
        D_fs.div_(torch.norm(D_fs, 2, 1, True))

        plt.imsave(img_root + "{}.png".format(k), (D_fs @ Q_fs.transpose(0, 1)).numpy())


def lasso_vpr_proof_of_concept_2():
    root = "./weights/vpr_lasso_3/"
    if "0.npy" not in list(os.listdir(root)):
        lasso_vpr(lambd_values=np.zeros(1))
    else:
        print("0.pny already created")
    Q, D, gt = data_vpr.vpr_data("bonn")
    D_exp = data_vpr.expand_ref(Q, D, gt)
    filepath = "../noseqslam/results/descriptors/"
    os.system("mkdir -p {}".format(filepath))
    filename = "radenovic{}_bonn"

    beta2 = torch.from_numpy(np.load(root + "0.npy")).reshape((2048, 1))
    Q = Q.type(beta2.dtype)
    mapped = Q @ beta2

    m_i_arr = [0, 1, 2, 541, 542, 543]

    for m_i in range(mapped.shape[0]):
        if m_i in m_i_arr:
            print("{:.10f}".format(mapped[m_i].item()))

    D_exp = D_exp.type(beta2.dtype)
    mapped = D_exp @ beta2

    m_i_arr = [0, 1, 2, 541, 542, 543]

    for m_i in range(mapped.shape[0]):
        if m_i in m_i_arr:
            print("{:.10f}".format(mapped[m_i].item()))


def lasso_vpr_proof_of_concept():
    root = "./weights/vpr_lasso_3/"
    if "0.npy" not in list(os.listdir(root)):
        lasso_vpr(lambd_values=np.zeros(1))
    else:
        print("0.pny already created")
    Q, D, gt = data_vpr.vpr_data("bonn")
    filepath = "../noseqslam/results/descriptors/"
    os.system("mkdir -p {}".format(filepath))
    filename = "radenovic{}_bonn"

    beta2 = torch.from_numpy(np.load(root + "0.npy"))
    beta1 = torch.ones(beta2.shape)

    betas = [beta1, beta2]
    for it, beta in enumerate(betas):
        print(filename.format(it))

        Q_fs = torch.mul(Q, beta.repeat((Q.shape[0], 1)))
        D_fs = torch.mul(D, beta.repeat((D.shape[0], 1)))

        Q_fs.div_(torch.norm(Q_fs, 2, 1, True))
        D_fs.div_(torch.norm(D_fs, 2, 1, True))

        plt.imsave("./images/vpr_lasso_proof/{}.png".format(it), (D_fs @ Q_fs.transpose(0, 1).numpy()))

        continue
        query_db_container = torch.jit.script(Container(Q_fs))
        ref_db_container = torch.jit.script(Container(D_fs))

        #shell_eval.clear_descriptors(on_desktop=False, hide_errors=True, home_replacement="/media/data1/jurica/")
        shell_eval.clear_descriptors(on_desktop=True, hide_errors=True, home_replacement="")

        torch.jit.save(query_db_container, "{}{}_query.pt".format(filepath, filename.format(it)))
        torch.jit.save(ref_db_container, "{}{}_reference.pt".format(filepath, filename.format(it)))

        #s = shell_eval.ml_shell_eval(on_desktop=False, home_replacement="", to_clear_data=False)
        s = shell_eval.ml_shell_eval(on_desktop=True, home_replacement="", to_clear_data=True, to_copy_pr_data=True)


def lasso_vpr_pipeline():
    weights_root = "./weights/vpr_lasso/"
    filepath = "../noseqslam/results/descriptors/"
    lambd_values = np.logspace(-2, 2, 40)

    Q, D, gt = data_vpr.vpr_data("freiburg")
    filename = "freiburg"

    for k, lambd_value in enumerate(lambd_values):

        print("k: {}, lambda: {:.5f}".format(k, lambd_value))

        beta = torch.from_numpy(np.load(weights_root + "{}.npy".format(k)))

        Q_fs = torch.mul(Q, beta.repeat((Q.shape[0], 1)))
        D_fs = torch.mul(D, beta.repeat((D.shape[0], 1)))

        query_db_container = torch.jit.script(Container(Q_fs))
        ref_db_container = torch.jit.script(Container(D_fs))

        shell_eval.clear_descriptors(on_desktop=False, hide_errors=True, home_replacement="/media/data1/jurica/")

        torch.jit.save(query_db_container, "{}{}_query.pt".format(filepath, filename))
        torch.jit.save(ref_db_container, "{}{}_reference.pt".format(filepath, filename))

        s = shell_eval.ml_shell_eval(on_desktop=False, home_replacement="/media/data1/jurica/")


if __name__ == "__main__":
    #lasso_vpr()
    #lasso_test()
    #lasso_vpr_pipeline()
    lasso_vpr_proof_of_concept()
    #lasso_vpr_proof_of_concept_2()
    #lasso_vpr(lambd_values=np.zeros(1))