from data_vpr import lasso_regression_data, logistic_regression_data, weakly_supervised_data
import numpy as np
from metric_learn import LMNN, ITML, SDML, MMC
import os
import datetime
import scipy


def learn_metric():
    X, y = lasso_regression_data()
    print(datetime.datetime.now())
    lmnn = LMNN(k=1, learn_rate=1e-4, verbose=True)
    lmnn.fit(X, y)

    M = lmnn.get_mahalanobis_matrix()

    root = "./weights/vpr_lmnn/"
    os.system("mkdir -p {}".format(root))
    np.save(root + "M2.npy", M)
    print(datetime.datetime.now())


def is_pos_def(x):
    return np.all(np.linalg.eigvals(x) >= 0)


def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)


def learn_metric_itml():
    X, pairs, y = weakly_supervised_data()

    print(datetime.datetime.now())
    itml = ITML(preprocessor=X, verbose=True)
    itml.fit(pairs, y)

    M = itml.get_mahalanobis_matrix()

    root = "./weights/vpr_lmnn/"
    os.system("mkdir -p {}".format(root))
    np.save(root + "M2.npy", M)
    print(datetime.datetime.now())


def learn_metric_sdml():
    X, pairs, y = weakly_supervised_data()

    print(datetime.datetime.now())
    sdml = SDML(preprocessor=X, verbose=True)
    sdml.fit(pairs, y)

    M = sdml.get_mahalanobis_matrix()

    root = "./weights/vpr_lmnn/"
    os.system("mkdir -p {}".format(root))
    np.save(root + "M2.npy", M)
    print(datetime.datetime.now())


def learn_metric_mmc():
    X, pairs, y = weakly_supervised_data()

    print(datetime.datetime.now())
    mmc = MMC(preprocessor=X, verbose=True)
    mmc.fit(pairs, y)

    M = mmc.get_mahalanobis_matrix()

    root = "./weights/vpr_lmnn/"
    os.system("mkdir -p {}".format(root))
    np.save(root + "M2.npy", M)
    print(datetime.datetime.now())


def is_pos_def(x):
    return np.all(np.linalg.eigvals(x) >= 0)


def check_symmetric(a, rtol=1e-05, atol=1e-08):
    return np.allclose(a, a.T, rtol=rtol, atol=atol)


def load_and_get_S():
    root = "./weights/vpr_lmnn/"
    M = np.load(root + "M3.npy")

    w, v = np.linalg.eigh(M)
    R1 = v @ np.diag(w.clip(min=0.0)) @ v.T
    R2 = v @ np.diag(w) @ v.T

    print(is_pos_def(R1))
    print(is_pos_def(R2))
    print(np.linalg.norm(M - R1))
    print(np.linalg.norm(M - R2))
    print(w.shape)
    print(v.shape)
    exit()
    e = np.linalg.eigvals(M)
    for e_i in range(e.shape[0]):
        if e[e_i] <= 0:
            print(e_i)
            print(e[e_i])

    for i in range(2048):
        for j in range(2048):
            if np.abs(M[i, j]) <= 1e-14:
                print(M[i, j])
    #exit()
    S = None
    if "S3.npy" not in list(os.listdir(root)):
        #S = scipy.linalg.sqrtm(M)
        S = scipy.linalg.cholesky(M)
        np.save(root + "S3.npy", S)
    else:
        S = np.load(root + "S3.npy")

    print(S)

if __name__ == "__main__":
    #learn_metric()
    #learn_metric_sdml()
    #learn_metric_mmc()
    #learn_metric_itml()

    load_and_get_S()