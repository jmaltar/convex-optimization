import os
import cvxpy as cp
import numpy as np
import torch
import matplotlib.pyplot as plt
import time
import datetime
import data_vpr_new as data_vpr
from pathlib import Path
from extractor import Container
import shell_eval


def loss_fn(X, Y, beta):
    return cp.pnorm(X @ beta - Y, p=2) ** 2


def regularizer(beta):
    # ridge
    #return cp.pnorm(beta, p=2) ** 2

    # lasso
    return cp.norm1(beta)


def objective_fn(X, Y, beta, lambd):
    return loss_fn(X, Y, beta) + lambd * regularizer(beta)


def lasso_vpr(root="./weights/vpr_lasso/", to_save=True, lambd_values=None):
    Path(root).mkdir(parents=True, exist_ok=True)
    X, y = data_vpr.lasso_regression_data(dataset_name="bonn")
    beta = cp.Variable(X.shape[1])

    lambd = cp.Parameter(nonneg=True)
    problem = cp.Problem(cp.Minimize(objective_fn(X, y, beta, lambd)))
    if lambd_values is None:
        lambd_values = np.logspace(-2, 2, 40)

    for k, lambd_value in enumerate(lambd_values):
        print("lasso_vpr: {}/{}".format(k + 1, len(lambd_values)))
        lambd.value = lambd_value
        problem.solve()
        if to_save:
            np.save(root + "{}.npy".format(k), beta.value)


def lasso_vpr_proof_of_concept():
    root = "./weights/vpr_lasso/"
    os.system("mkdir -p {}".format(root))

    if "0.npy" not in list(os.listdir(root)):
        lasso_vpr(lambd_values=np.zeros(1))
    else:
        print("0.pny already created")

    Q, D, gt = data_vpr.vpr_data("bonn")
    fmpath = "/data/storage/jurica/feature_maps_co/"
    os.system("mkdir -p {}".format(fmpath))

    filename = "radenovic{}_bonn"

    beta2 = torch.from_numpy(np.load(root + "0.npy"))
    beta1 = torch.ones(beta2.shape)

    betas = [beta1, beta2]
    for it, beta in enumerate(betas):

        Q_fs = torch.mul(Q, beta.repeat((Q.shape[0], 1)))
        D_fs = torch.mul(D, beta.repeat((D.shape[0], 1)))

        Q_fs.div_(torch.norm(Q_fs, 2, 1, True))
        D_fs.div_(torch.norm(D_fs, 2, 1, True))

        query_db_container = torch.jit.script(Container(Q_fs))
        ref_db_container = torch.jit.script(Container(D_fs))

        os.system("rm -rf {}*".format(fmpath))

        torch.jit.save(query_db_container, "{}{}_query.pt".format(fmpath, filename.format(it)))
        torch.jit.save(ref_db_container, "{}{}_reference.pt".format(fmpath, filename.format(it)))
        os.system("cd /data/storage/jurica/nco/cmake-build-debug/; ./noseqslam")
        os.system("cd /data/storage/jurica/convex-optimization/")


if __name__ == "__main__":
    lasso_vpr_proof_of_concept()