import enum

class database_type(enum.Enum):
    any = 0
    query = 1
    reference = 2


class dataset_name(enum.Enum):
    Bonn = 0
    Freiburg = 1


class train_variant(enum.Enum):
    single = 0
    multiple = 1


class model_name(enum.Enum):
    AlexNet = 0
    ResNet50 = 1
    ResNet18 = 2
    AlexNet3 = 3
    AlexNet4 = 4


class criterion_name(enum.Enum):
    ContrastiveLoss = 0


class optimizer_name(enum.Enum):
    SGD = 0
    Adam = 1


if __name__ == "__main__":
    d = dataset_name.Freiburg
    print(str(d).split(".")[-1])