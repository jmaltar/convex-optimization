from cnns.ConvOnlyNet import ConvOnlyNet
import cnns.alexnet
import cnns.resnet50
import extractor
import torch

import const

nets = {
    const.model_name.AlexNet3: cnns.alexnet.layers()[3],
    const.model_name.AlexNet4: cnns.alexnet.layers()[4],
    const.model_name.ResNet50: cnns.resnet50.layers()[49]
}

datasets = [const.dataset_name.Bonn, const.dataset_name.Freiburg]
database_types = [const.database_type.query, const.database_type.reference]


def format_dataset_path(dataset_name, dataset_type): return "../data/{}_example_new/{}/images/".format(dataset_name, dataset_type)


def extract_feature_maps():
    for net_name, net in nets.items():
        for dataset_name in datasets:
            for database_type in database_types:
                dataset_path = format_dataset_path(str(dataset_name).split(".")[-1].lower(), str(database_type).split(".")[-1])
                t = extractor.extract_by_model(ConvOnlyNet(nets[net_name]), to_cuda=True, dataset_name=dataset_name, database_type=database_type)
                torch.save(t, "./tensors/{}_{}_{}.pt".format(str(dataset_name).split(".")[-1].lower(), str(database_type).split(".")[-1], str(net_name).split(".")[-1].lower()))
                print(t.shape)
                #print(dataset_path)


if __name__ == "__main__":
    extract_feature_maps()