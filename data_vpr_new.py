import torch
import numpy as np
import matplotlib.pyplot as plt
import extractor
import const


def load_gt(gt_filepath="../data/bonn/gt_bonn.out"):
    gt = []
    with open(gt_filepath) as fl:
        for ln in fl:
            ln = ln.replace("\n", "")
            gts = ln.split(" ")[2:]
            if gts[-1] == "":
                gts.pop()

            gt.append([int(g) for g in gts])
    return gt


def expand_ref(query_db, ref_db, gt):
    query_size = query_db.size(0)
    reference_size = ref_db.size(0)

    query_db_new = query_db.clone()
    query_db_new.div_(torch.norm(query_db_new, 2, 1, True))
    ref_db_new = ref_db.clone()
    ref_db_new.div_(torch.norm(ref_db_new, 2, 1, True))

    gt_similarities = query_db_new.matmul(ref_db_new.transpose(0, 1))

    for i in range(query_size):
        for j in range(reference_size):
            if j not in gt[i]:
                gt_similarities[i][j] = 0.0

    f_size = query_db_new.size(1)
    t_best = torch.argsort(gt_similarities, -1, True).transpose(0, 1)[0]
    t_best = t_best.reshape((t_best.size(0), 1))
    t_best = t_best.repeat((1, f_size))
    return ref_db.gather(0, t_best)


def vpr_data(dataset_name="bonn", net_name="resnet101radenovicpython", plain=False):
    Q = D = None
    if plain:
        Q = extractor.extract_plain_tensors(const.dataset_name[dataset_name.capitalize()], const.database_type.query)
        D = extractor.extract_plain_tensors(const.dataset_name[dataset_name.capitalize()], const.database_type.reference)
    else:
        Q = torch.load("./tensors/{}_query_{}.pt".format(dataset_name, net_name))
        D = torch.load("./tensors/{}_reference_{}.pt".format(dataset_name, net_name))

    Q.div_(torch.norm(Q, 2, 1, True))
    D.div_(torch.norm(D, 2, 1, True))

    gt_filepath = "../data/{}/gt_{}.out".format(dataset_name, dataset_name)
    gt = load_gt(gt_filepath)

    return Q, D, gt


def bonn_data(net_name="resnet101radenovicpython"):
    return vpr_data("bonn", net_name=net_name)


def freiburg_data(net_name="resnet101radenovicpython"):
    return vpr_data("freiburg", net_name=net_name)


def lasso_regression_data(dataset_name="bonn", net_name="resnet101radenovicpython"):
    Q, D, gt = vpr_data(dataset_name, net_name)
    D_exp = expand_ref(Q, D, gt)
    Q = Q.numpy()
    D_exp = D_exp.numpy()

    X = np.concatenate([Q, D_exp])
    y = np.concatenate([range(Q.shape[0]), range(D_exp.shape[0])]).astype(Q.dtype)

    return X, y


def bonn_lasso_regression_data(net_name="resnet101radenovicpython"):
    return lasso_regression_data("bonn", net_name=net_name)


def freiburg_lasso_regression_data(net_name="resnet101radenovicpython"):
    return lasso_regression_data("freiburg", net_name=net_name)


def logistic_regression_data(dataset_name="bonn", net_name="resnet101radenovicpython", subset_Q=None, subset_D=None, plain=False):
    Q, D, gt = vpr_data(dataset_name, net_name, plain=plain)

    Q = Q.numpy()
    D = D.numpy()

    if subset_Q and subset_D:
        D = D[:subset_D]
        gt_new = []
        q_i = 0
        for gt_line in gt:
            gt_line_new = []
            for r_i in gt_line:
                if r_i < subset_D:
                    gt_line_new.append(r_i)

            if len(gt_line_new) > 0:
                q_i += 1
                gt_new.append(gt_line_new)
            else:
                subset_Q = q_i
                break

        Q = Q[:subset_Q]
        gt = gt_new

        GT = np.zeros((D.shape[0], Q.shape[0]), dtype=Q.dtype)
        for gt_i in range(GT.shape[0]):
            for gt_j in range(GT.shape[1]):
                if gt_i in gt[gt_j]:
                    GT[gt_i, gt_j] = 1.0

    len_Q = Q.shape[0]
    len_D = D.shape[0]
    c = len_D
    n = Q.shape[1]

    y_Q = np.zeros((len_Q, len_D), dtype=Q.dtype)
    for i, gt_line in enumerate(gt):
        for j in gt_line:
            y_Q[i, j] = 1.0

    y_D = np.eye(len_D, dtype=Q.dtype)

    y = np.concatenate([y_Q, y_D])
    X = np.concatenate([Q, D])

    return X, y, n, c


def weakly_supervised_data(dataset_name="bonn", net_name="resnet101radenovicpython", plain=False):
    Q, D, gt = vpr_data(dataset_name, net_name, plain=plain)

    len_Q = Q.shape[0]
    len_D = D.shape[0]

    pairs = np.zeros((len_Q * len_D, 2), dtype=np.int)
    y = np.zeros(len_Q * len_D)
    X = torch.cat([Q, D], dim=0).numpy()

    it = 0
    for q_i in range(len_Q):
        for d_j in range(len_D):
            pairs[it][0] = q_i
            pairs[it][1] = len_Q + d_j
            if d_j in gt[q_i]:
                y[it] = 1
            else:
                y[it] = -1
            it += 1
    return X, pairs, y


if __name__ == "__main__":
    0