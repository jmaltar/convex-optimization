import os


def shell_eval(commands, on_desktop=True, home_replacement=""):
    os.system("touch ~/colog; > ~/colog")

    for command in commands:

        command_ = "{}{}{}".format(command[0], " > /dev/null" if command[1] else "", " 2>&1" if command[3] else "")
        if home_replacement != "":
            command_ = command_.replace("~/", home_replacement)

        if command[2]:
            command_ += " | tee ~/colog"
        if not on_desktop:
            command_ = command_.replace("/Desktop", "")
        os.system(command_)

    with open(os.path.expanduser("~/colog")) as f:
        lines = f.readlines()
        if lines.__len__() > 0:
            lines[-1] = lines[-1].replace("\n", "")
        s = "".join(lines)

    return s


def clear_descriptors(on_desktop=False, hide_errors=False, home_replacement="", folder_name="noseqslam"):
    commands = [
        ("rm ~/Desktop/" + folder_name + "/results/descriptors/*", True, False, hide_errors),
    ]
    return shell_eval(commands, on_desktop, home_replacement)


def clear_data(on_desktop=False, hide_errors=False, home_replacement="", folder_name="noseqslam"):
    commands = [
        ("rm ~/Desktop/" + folder_name + "/results/precision_and_recall/metric_learning_continuous/*", True, False, hide_errors),
        ("rm ~/Desktop/" + folder_name + "/results/metric_learning_continuous/*/matrices/*", True, False, hide_errors),
        ("rm ~/Desktop/" + folder_name + "/results/noseqslam/metric_learning_continuous/matrices/*/*", True, False, hide_errors),
        ("rm ~/Desktop/" + folder_name + "/results/seqslam/metric_learning_continuous/matrices/*/*", True, False, hide_errors),
        ("rm ~/Desktop/" + folder_name + "/results/seqslamcone/metric_learning_continuous/matrices/*/*", True, False, hide_errors)
    ]
    return shell_eval(commands, on_desktop, home_replacement)


def copy_pr_data(on_desktop=False, hide_errors=False, home_replacement=""):
    commands = [
        ("mkdir -p ~/metric_learning_continuous/", True, False, hide_errors),
        ("cp ~/Desktop/noseqslam/results/precision_and_recall/metric_learning_continuous/* ~/metric_learning_continuous/", True, False, hide_errors),
    ]
    return shell_eval(commands, on_desktop, home_replacement)


def ml_shell_eval(on_desktop=False, home_replacement="", to_clear_data=True, to_copy_pr_data=False, folder_name="noseqslam"):
    commands = [
        ("cd ~/Desktop/" + folder_name + "/cmake-build-debug/; ./noseqslam", True, False, False),
        ("cd ~/Desktop/precision-recall-plotter; python3 ./main_plots.py", False, True, False),
    ]

    clear_data(on_desktop, hide_errors=True, home_replacement=home_replacement, folder_name=folder_name)
    s = shell_eval(commands, on_desktop, home_replacement=home_replacement)


    if to_copy_pr_data:
        copy_pr_data(on_desktop=on_desktop, hide_errors=False, home_replacement=home_replacement)

    if to_clear_data:
        clear_descriptors(on_desktop, home_replacement=home_replacement)
        clear_data(on_desktop, hide_errors=False, home_replacement=home_replacement)
    return s




if __name__ == "__main__":
    s = ml_shell_eval(on_desktop=False, home_replacement="/media/data1/jurica/")
    #print(s)



