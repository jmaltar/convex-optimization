import torchvision


def layers(pretrained=True):
    resnet101 = torchvision.models.resnet101(pretrained=pretrained)

    # fetch all layers (excluding last average pooling & fully connected layer)
    resnet101_all_layers = list(resnet101.children())[:-2]
    resnet101_layers = {
        100: resnet101_all_layers[:8]
    }
    return resnet101_layers


if __name__ == "__main__":
    print(layers()[100].__len__())
