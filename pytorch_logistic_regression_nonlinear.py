import torch
from torch.utils.data import DataLoader
from dataset import DatasetSupervised
from torchvision.models import resnet18
from torchvision.models import alexnet
from data_vpr import logistic_regression_data, vpr_data
import numpy as np
import matplotlib.pyplot as plt
import os
from extractor import Container
import shell_eval
import torch.nn as nn

def plot_W_Q(W, Q, epoch_i, root):
    W.eval()
    W = W.cpu()
    with torch.set_grad_enabled(False):
        plt.imsave("./images/softmax/{}.png".format(epoch_i + 1), W(Q).transpose(0, 1).detach().numpy())
    W = W.cuda()
    W.train()


def logistic_vpr_nonlinear():
    use_plain_data = False
    X, y, n, c = logistic_regression_data(plain=use_plain_data)
    Q, D, gt = vpr_data(plain=use_plain_data)

    X = torch.from_numpy(X)
    y = torch.from_numpy(y)
    y.div_(torch.sum(y, dim=0, keepdim=True))

    #W = torch.nn.Linear(n, c)
    W = alexnet(pretrained=True)
    W.classifier = nn.Sequential(
        nn.Dropout(),
        nn.Linear(256 * 6 * 6, 4096),
        nn.ReLU(inplace=True),
        nn.Dropout(),
        nn.Linear(4096, 4096),
        nn.ReLU(inplace=True),
        nn.Linear(4096, c),
    )

    S = torch.nn.Softmax(dim=1)

    to_cuda = True
    if to_cuda:
        X = X.to(torch.device("cuda:0"))
        y = y.to(torch.device("cuda:0"))
        W = W.to(torch.device("cuda:0"))
        S = S.to(torch.device("cuda:0"))

    ds = DatasetSupervised(X, y)
    dataloader = DataLoader(ds, batch_size=4, shuffle=True)
    optimizer = torch.optim.SGD(W.parameters(), lr=0.01, momentum=0.9)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)

    num_epochs = 100

    os.system("rm -rf ./images/softmax/")
    os.system("mkdir -p ./images/softmax/")

    plot_W_Q(W, Q, -1)

    for epoch_i in range(num_epochs):
        epoch_loss = 0
        for batch_i, batch in enumerate(dataloader):
            X_batch = batch[0]
            y_batch = batch[1]
            optimizer.zero_grad()

            with torch.set_grad_enabled(True):
                loss = -torch.sum(torch.multiply(y_batch, torch.log(S(W(X_batch)))))
                loss.backward()
                optimizer.step()
            epoch_loss += loss.item()
        scheduler.step()

        if (epoch_i + 1) % 1 == 0:
            plot_W_Q(W, Q, epoch_i)
            print(epoch_loss)
    torch.save(W, "./weights/vpr_logistic_1/0.pt")


def logistic_regression_vpr_proof_of_concept():
    root = "./weights/vpr_logistic_1/"
    if "0.pt" not in list(os.listdir(root)):
        logistic_vpr_nonlinear()
    else:
        print("0.pt already created")

    W = torch.load(root + "0.pt")
    W.eval()
    W = W.cpu()

    Q, D, gt = vpr_data(plain=False)
    Q_fabricated = W(Q).detach().numpy()
    D_fabricated = np.eye(Q_fabricated.shape[1])

    filepath = "../noseqslam/results/descriptors/"
    os.system("mkdir -p {}".format(filepath))
    filename = "radenovic{}_bonn"

    Q_fabricated = torch.from_numpy(Q_fabricated).type(torch.float32)
    D_fabricated = torch.from_numpy(D_fabricated).type(torch.float32)

    vpr_data_list = [(Q, D), (Q_fabricated, D_fabricated)]

    for l_i, l_item in enumerate(vpr_data_list):
        Q_fs, D_fs = l_item
        plt.imsave("./images/vpr_logistic_proof/{}.png".format(l_i), (D_fs @ Q_fs.transpose(0, 1).numpy()))
        query_db_container = torch.jit.script(Container(Q_fs))
        ref_db_container = torch.jit.script(Container(D_fs))

        shell_eval.clear_descriptors(on_desktop=True, hide_errors=True, home_replacement="")

        torch.jit.save(query_db_container, "{}{}_query.pt".format(filepath, filename.format(l_i)))
        torch.jit.save(ref_db_container, "{}{}_reference.pt".format(filepath, filename.format(l_i)))

        s = shell_eval.ml_shell_eval(on_desktop=True, home_replacement="", to_clear_data=True, to_copy_pr_data=True)


if __name__ == "__main__":
    logistic_vpr_nonlinear()
