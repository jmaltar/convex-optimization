import pickle
import numpy as np

with open("data/mnist/pickled_mnist.pkl", "br") as fh:
    data = pickle.load(fh)
train_imgs = data[0]
test_imgs = data[1]
train_labels = data[2]
test_labels = data[3]


def create_zero_not_zero_data(X_train, y_train):
    w = np.zeros(X_train[0].shape[0])
    X = np.array(X_train)
    y = np.array(y_train)
    y = y.reshape(np.dot(X, w).shape)
    for i in range(y.shape[0]):
        if y[i] == 0.0:
            y[i] = 1
        else:
            y[i] = 0
    return X, y


def create_zero_one_data(X_train, y_train):
    X_list = []
    y_list = []

    for i, y_i in enumerate(y_train):
        if y_i == 0 or y_i == 1:
            if y_i == 0:
                y_list.append(1)
            elif y_i == 1:
                y_list.append(0)
            X_list.append(X_train[i])
    X = np.array(X_list)
    y = np.array(y_list)
    return X, y


def create_multiclass_data(X_train, y_train, K=10):
    X = np.array(X_train)
    y = np.zeros((X_train.shape[0], K))
    for i, y_i in enumerate(y_train):
        temp = np.zeros(K)
        temp[int(y_i)] = 1
        y[i] = temp

    return X, y


def get_X_and_y():
    X, y = create_multiclass_data(train_imgs, train_labels)
    return X, y


if __name__ == "__main__":
    X, y = get_X_and_y()
    print(X.shape)
    print(y.shape)

