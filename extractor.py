import numpy as np
import torch
from torch.utils.data import DataLoader
#from net import get_model
from dataset import DatasetVPR
import const
import os


def forward_propagation(model, num_data, out_dim, dataloader, to_cuda, dtype):
    z = torch.zeros((num_data, out_dim), dtype=dtype)
    if to_cuda:
        z = z.to(torch.device("cuda:0"))

    c = 0
    for i, samples in enumerate(dataloader):
        x_batch = samples[0]
        if to_cuda:
            x_batch = x_batch.to(torch.device("cuda:0"))
        with torch.set_grad_enabled(False):
            z_batch = model(x_batch)
            z[c: c + z_batch.shape[0]] = z_batch.requires_grad_(False).flatten(1)
            c += z_batch.shape[0]
    return z.cpu()


def forward_propagation_no_out_dim(model, dataloader, to_cuda):
    z = None
    for i, samples in enumerate(dataloader):
        x_batch = samples[0]
        batch_size = x_batch.size(0)
        if to_cuda:
            x_batch = x_batch.to(torch.device("cuda:0"))
        with torch.set_grad_enabled(False):
            z_batch = model(x_batch)
            feature_vectors = z_batch.view(batch_size, -1)
            if z is None:
                z = feature_vectors
            else:
                z = torch.cat((z, feature_vectors), 0)
    return z.cpu()


def plain_tensors(dataloader):
    x = None
    for i, samples in enumerate(dataloader):
        x_batch = samples[0]
        batch_size = x_batch.size(0)
        with torch.set_grad_enabled(False):
            feature_vectors = x_batch.view(batch_size, -1)
            if x is None:
                x = feature_vectors
            else:
                x = torch.cat((x, feature_vectors), 0)
    return x.cpu()


def extract_by_model(model, to_cuda=True, dataset_name=const.dataset_name.Freiburg, database_type=const.database_type.reference):
    if to_cuda and torch.cuda.is_available():
        model.to(torch.device("cuda:0"))
    model.eval()

    dataset = DatasetVPR(dataset_name=dataset_name, to_cuda=to_cuda, train_variant=const.train_variant.single, database_type=database_type)
    dataloader = DataLoader(dataset, batch_size=4, shuffle=False, num_workers=0)

    # forward propagation
    z = forward_propagation_no_out_dim(model, dataloader, to_cuda)
    return z


def extract_plain_tensors(dataset_name=const.dataset_name.Freiburg, database_type=const.database_type.reference):
    dataset = DatasetVPR(dataset_name=dataset_name, to_cuda=False, train_variant=const.train_variant.single, database_type=database_type)
    dataloader = DataLoader(dataset, batch_size=4, shuffle=False, num_workers=0)
    return plain_tensors(dataloader)


class Container(torch.nn.Module):
    def __init__(self, t):
        super().__init__()
        self.tensor = torch.nn.Parameter(t)


if __name__ == "__main__":
    print("Go to ./extract_features.py")