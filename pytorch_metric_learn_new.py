import os
import torch
import torch.nn.functional as F
import data_vpr_new as data_vpr
from datetime import datetime
from torch.utils.data import DataLoader
from dataset import DatasetVPRLite
from extractor import Container
import matplotlib.pyplot as plt
import shell_eval

def learn_metric():
    Q, D, gt = data_vpr.vpr_data()
    n = Q.shape[1]
    L = torch.nn.Linear(n, n)

    to_cuda = True
    if to_cuda:
        Q = Q.to(torch.device("cuda:0"))
        D = D.to(torch.device("cuda:0"))
        L = L.to(torch.device("cuda:0"))

    ds = DatasetVPRLite(Q, D, gt)
    dataloader = DataLoader(ds, batch_size=256, shuffle=True)
    optimizer = torch.optim.SGD(L.parameters(), lr=0.01, momentum=0.9)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)

    num_epochs = 100

    os.system("rm -rf ./images/metric_learning/")
    os.system("mkdir -p ./images/metric_learning/")

    #plot_W_Q(L, Q, -1, root="./images/metric_learning/")

    for epoch_i in range(num_epochs):
        epoch_loss = 0
        for batch_i, batch in enumerate(dataloader):
            Q_batch = batch[0]#.to(torch.device("cuda:0"))
            D_batch = batch[1]#.to(torch.device("cuda:0"))
            y_batch = batch[2].to(torch.device("cuda:0"))
            optimizer.zero_grad()
            with torch.set_grad_enabled(True):
                distances = (F.normalize(L(Q_batch)) - F.normalize(L(D_batch))).pow(2).sum(1, keepdim=False).sqrt()
                losses = y_batch * distances.pow(2) + (1 - y_batch) * F.relu(1 - distances).pow(2)
                loss = losses.mean()
                loss.backward()
                optimizer.step()
            epoch_loss += loss.item()
        scheduler.step()

        if (epoch_i + 1) % 1 == 0:
            info_str = "Epoch {}/{}; Loss: {}; Time: {}".format(epoch_i + 1, num_epochs, epoch_loss, datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            print(info_str)
            #plot_W_Q(L, Q, epoch_i, root="./images/metric_learning/")
            #print(epoch_loss)

    os.system("mkdir -p ./weights/vpr_metric_learning/")
    torch.save(L, "./weights/vpr_metric_learning/L.pt")

def metric_learning_proof_of_concept():
    root = "./weights/vpr_metric_learning/"
    if "L.pt" not in list(os.listdir(root)):
        learn_metric()
    else:
        print("L.pt already created")

    L = torch.load(root + "L.pt")
    L.eval()
    L = L.cpu()

    Q, D, gt = data_vpr.vpr_data(plain=False)
    Q_mapped = L(Q).detach().type(torch.float)
    D_mapped = L(D).detach().type(torch.float)

    os.system("rm -rf /data/storage/jurica/nco/results/")

    fmpath = "/data/storage/jurica/feature_maps_co/"
    os.system("mkdir -p {}".format(fmpath))

    filename = "radenovic{}_bonn"

    vpr_data_list = [(Q, D), (Q_mapped, D_mapped)]

    for l_i, l_item in enumerate(vpr_data_list):
        Q_fs, D_fs = l_item
        #plt.imsave("./images/vpr_metric_learning_proof/{}.png".format(l_i), (D_fs @ Q_fs.transpose(0, 1).numpy()))
        query_db_container = torch.jit.script(Container(Q_fs))
        ref_db_container = torch.jit.script(Container(D_fs))

        shell_eval.clear_descriptors(on_desktop=True, hide_errors=True, home_replacement="")
        os.system("rm -rf {}*".format(fmpath))
        torch.jit.save(query_db_container, "{}{}_query.pt".format(fmpath, filename.format(l_i)))
        torch.jit.save(ref_db_container, "{}{}_reference.pt".format(fmpath, filename.format(l_i)))

        os.system("cd /data/storage/jurica/nco/cmake-build-debug/; ./noseqslam")
        os.system("cd /data/storage/jurica/convex-optimization/")


if __name__ == "__main__":
    metric_learning_proof_of_concept()