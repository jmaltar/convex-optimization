import torch
from torch.utils.data import Dataset, DataLoader
from torchvision.transforms import transforms
from PIL import Image
import math
import os
import const

transform_ = transforms.Compose(
        [
            transforms.Resize(32),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        ]
    )


class DatasetSupervised(Dataset):

    __slots__ = ["X", "y", "length"]

    def __init__(self, X, y):
        self.X = X
        self.y = y
        self.length = X.shape[0]

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        return self.X[item], self.y[item]



class DatasetVPR(Dataset):
    __slots__ = [
        "len_Q", "len_D", "len_single", "len_multiple",
        "filepaths", "train_variant", "correspondence",
        "neighborhood_radius", "transform",
        "images", "in_advance", "to_cuda",
        "dataset_name", "significance", "exact"
    ]

    @staticmethod
    def item_to_i_j(item, n):
        # maps index in range from 0 to n * (n - 1) / 2 - 1 to pair of indices from 0 to n - 1 (k < j)
        k = int(math.ceil(-((3 - 2 * n) / 2 + math.sqrt(((3 - 2 * n) / 2) ** 2 + 2 * (n - 2 - item)))))
        delta = item - k * n + k * (k + 1) / 2
        j = int(k + 1 + delta)
        return k, j

    @staticmethod
    def item_to_i_j_exact(item, len_D, len_Q):
        i = item // len_D
        j = (item % len_D) + len_Q
        return i, j

    def __init__(self, dataset_name=const.dataset_name.Bonn, train_variant=const.train_variant.multiple,
                 neighborhood_radius=0, transform=transform_, in_advance=False, to_cuda=False, database_type=const.database_type.any, exact=False
                 ):
        self.dataset_name = dataset_name
        # perform either a full siamese training or a pdist variant
        self.train_variant = train_variant
        # neighborhood images considered
        self.neighborhood_radius = neighborhood_radius
        # as usual for images
        self.transform = transform
        # load the data in advance on RAM/GPU
        self.in_advance = in_advance
        # upload on GPU
        self.to_cuda = to_cuda

        self.exact = exact

        db_name = "bonn" if dataset_name == const.dataset_name.Bonn else "freiburg"
        root = "../data/{}_example_new/".format(db_name)
        #root = "../data2/{}_example_new/".format(db_name)
        query_filepaths = sorted(["{}query/images/".format(root) + filepath for filepath in os.listdir("{}query/images/".format(root))])
        reference_filepaths = sorted(["{}reference/images/".format(root) + filepath for filepath in os.listdir("{}reference/images/".format(root))])

        if database_type == database_type.query:
            reference_filepaths = []
        elif database_type == database_type.reference:
            query_filepaths = []

        self.len_Q = len(query_filepaths)
        self.len_D = len(reference_filepaths)
        self.len_single = self.len_Q + self.len_D
        self.len_multiple = int(self.len_single * (self.len_single - 1) / 2)
        self.filepaths = query_filepaths + reference_filepaths

        gt_filepath = root + "gt_{}_example.out".format(db_name)
        with open(gt_filepath, "r") as gt_file:
            gt = []
            gt_line = gt_file.readline()
            gt_line = gt_line.replace(" \n", "")
            gt_line = gt_line.replace("\n", "")
            gt.append([int(i) for i in gt_line.split(" ")[2:]])
            while gt_line:

                gt_line = gt_file.readline()
                gt_line = gt_line.replace(" \n", "")
                gt_line = gt_line.replace("\n", "")
                if gt_line != "":
                    gt.append([int(i) for i in gt_line.split(" ")[2:]])

        # build the correspondence matrix that says whether I_q_i corresponds to I_d_j
        # also, when tweaking neighborhood_radius, whether I_q_i is near I_q_k as well as I_d_l is near I_d_m
        self.correspondence = torch.zeros((self.len_single, self.len_single))
        for item in range(0, self.len_multiple):
            i, j = DatasetVPR.item_to_i_j(item, self.len_single)
            if self.len_Q <= i or j < self.len_Q:
                self.correspondence[i, j] = abs(i - j) <= self.neighborhood_radius
            else:
                self.correspondence[i, j] = (j - self.len_Q) in gt[i]
        # make it symmetric
        self.correspondence = self.correspondence + self.correspondence.transpose(0, 1) - torch.diag(self.correspondence.diag())

        self.significance = torch.zeros((self.len_single, self.len_single))



        for i in range(0, self.len_single):
            for j in range(0, self.len_single):
                if i != j:
                    """
                    if i < self.len_Q <= j or j < self.len_Q <= i:
                        self.significance[i, j] = 1.0
                    """
                    self.significance[i, j] = 1.0

        r = self.neighborhood_radius
        if r == 0:
            r = 1
        for i in range(0, self.len_single):
            for j in range(0, self.len_single):
                if (abs(i - j) <= r):
                    self.significance[i, j] = 1.0 / r

        for i in range(0, self.len_single):
            for j in range(0, self.len_single):
                if i < self.len_Q <= j or j < self.len_Q <= i:
                    self.significance[i, j] = 1.0


        """
        plt.imsave("c.png", self.correspondence.cpu().numpy())
        plt.imsave("s.png", self.significance.cpu().numpy())
        """

        if to_cuda and torch.cuda.is_available():
            self.correspondence = self.correspondence.to(torch.device("cuda:0"))
            self.significance = self.significance.to(torch.device("cuda:0"))

        self.images = None

        if self.in_advance:
            image = Image.open(self.filepaths[0])
            if self.transform is not None:
                image = self.transform(image)
            self.images = torch.Tensor(self.len_single, image.shape[0], image.shape[1], image.shape[2])

            for i, filepath in enumerate(self.filepaths):
                image = Image.open(filepath)
                if self.transform is not None:
                    image = self.transform(image)
                self.images[i] = image
            if self.to_cuda and torch.cuda.is_available():
                self.images = self.images.to(torch.device("cuda:0"))

    def __len__(self):
        if self.train_variant == const.train_variant.single:
            return self.len_single
        elif self.train_variant == const.train_variant.multiple:
            if self.exact:
                return self.len_Q * self.len_D
            else:
                return self.len_multiple

    def __getitem__(self, item):
        if self.train_variant == const.train_variant.multiple:
            if self.exact:
                i, j = DatasetVPR.item_to_i_j_exact(item, len_D=self.len_D, len_Q=self.len_Q)
            else:
                i, j = DatasetVPR.item_to_i_j(item, self.len_single)

            if self.in_advance:
                x_i = self.images[i]
                x_j = self.images[j]
            else:
                x_i = Image.open(self.filepaths[i])
                x_j = Image.open(self.filepaths[j])
                if self.transform is not None:
                    x_i = self.transform(x_i)
                    x_j = self.transform(x_j)
            return x_i, x_j, i, j

        elif self.train_variant == const.train_variant.single:
            if self.in_advance:
                x_i = self.images[item]
            else:
                x_i = Image.open(self.filepaths[item])
                if self.transform is not None:
                    x_i = self.transform(x_i)
            return x_i, item


class DatasetVPRLite(Dataset):
    __slots__ = ["len_Q", "len_D", "length", "Q", "D", "gt", "mapper"]

    def item_to_i_j(self, item):
        return self.mapper[item]

    def __len__(self):
        return self.length

    def __init__(self, Q, D, gt):
        self.Q = Q
        self.D = D
        self.gt = gt
        self.len_Q = Q.shape[0]
        self.len_D = D.shape[0]
        self.length = self.len_Q * self.len_D

        os.system("mkdir -p ./weights/vpr_metric_learning/")
        if "mapper.npy" not in os.listdir("./weights/vpr_metric_learning/"):
            self.mapper = torch.zeros((self.length, 2), dtype=torch.long)
            it = 0
            for q_i in range(self.len_Q):
                for d_j in range(self.len_D):
                    self.mapper[it][0] = q_i
                    self.mapper[it][1] = d_j
                    it += 1
            torch.save(self.mapper, "./weights/vpr_metric_learning/mapper.npy")
        else:
            self.mapper = torch.load("./weights/vpr_metric_learning/mapper.npy")

    def __getitem__(self, item):
        """
        m = self.item_to_i_j(item)
        q_indices = m[:, 0]
        d_indices = m[:, 1]

        y = torch.zeros(item.shape[0], dtype=self.Q.dtype)
        for k in range(item.shape[0]):
            y[k] = 1.0 if int(d_indices[k]) in gt[int(q_indices[k])] else 0.0

        return self.Q[q_indices], self.D[d_indices], y
        """
        m = self.item_to_i_j(item)
        q_i = m[0]
        d_j = m[1]
        y = 1.0 if int(d_j) in self.gt[int(q_i)] else 0.0
        return self.Q[q_i], self.D[d_j], y

if __name__ == '__main__':
    #d = DatasetVPR(neighborhood_radius=0, train_variant=const.train_variant.multiple, exact=True, in_advance=True, to_cuda=False)
    #dataloader = DataLoader(d, batch_size=8, shuffle=False, num_workers=0)
    Q, D, gt = data_vpr.vpr_data()

    ds = DatasetVPRLite(Q, D, gt)
    Q_batch, D_batch, y_batch = ds[torch.tensor(list(range(544)), dtype=torch.long)]
