import numpy as np
import cvxpy as cp
import datetime

import data_mnist
import data_vpr


# https://math.stackexchange.com/questions/3427763/convexity-of-softmax-logistic-regression
def neg_log_softmax(x):
    return cp.log_sum_exp(x, axis=1, keepdims=True) - x


# http://deeplearning.stanford.edu/tutorial/supervised/SoftmaxRegression/
def cost(X, y, W):
    P = X @ W.T
    L_neg = neg_log_softmax(P)
    return cp.sum(cp.multiply(y, L_neg))


def random_example():
    np.random.seed(1)
    c = 5
    n = 7
    m = 10
    y = np.random.rand(m, c)
    y_sums = y.sum(axis=1)
    y_n = y / y_sums[:, np.newaxis]
    y = y_n
    X = np.random.randn(m, n)
    W = cp.Variable((c, n))
    objective = cp.Minimize(cost(X, y, W))
    problem = cp.Problem(objective)
    problem.solve()
    np.save("./weights/W_random.npy", W.value)
    print(W.value)
    print(problem.status)
    print(datetime.datetime.now())


def mnist_example():
    # training only
    X, y = data_mnist.get_X_and_y()
    c = 10
    n = 28 * 28
    W = cp.Variable((c, n))
    objective = cp.Minimize(cost(X, y, W))
    problem = cp.Problem(objective)
    problem.solve()
    print(problem.status)
    print(problem.value)
    np.save("./weights/W_mnist.npy", W.value)
    print(datetime.datetime.now())


def vpr_example():
    X, y, n, c = data_vpr.bonn_logistic_regression_data(subset_Q=300, subset_D=200)

    m = X.shape[0]
    print(n)
    print(c)
    print(m)

    W = cp.Variable((c, n))
    objective = cp.Minimize(cost(X, y, W))
    problem = cp.Problem(objective)
    problem.solve()
    print(problem.status)
    print(problem.value)
    np.save("./weights/W_vpr.npy", W.value)
    print(datetime.datetime.now())

if __name__ == "__main__":
    #random_example()
    #mnist_example()
    vpr_example()





